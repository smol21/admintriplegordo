import "core-js/stable";
import Vue from "vue";
import VueSweetalert2 from "vue-sweetalert2";
import App from "./App";
import { iconsSet as icons } from "./assets/icons/icons.js";
import router from "./router";
import CoreuiVue from "@coreui/vue";
import store from "./store";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import "sweetalert2/src/sweetalert2.scss";
import Vuelidate from "vuelidate";
import Loading from "vue-loading-overlay";
import "vue-loading-overlay/dist/vue-loading.css";
import VueToastr2 from "vue-toastr-2";
import "vue-toastr-2/dist/vue-toastr-2.min.css";
import VueFormWizard from "vue-form-wizard";
import "vue-form-wizard/dist/vue-form-wizard.min.css";
import VueFlicking from "@egjs/vue-flicking";
import "@egjs/vue-flicking/dist/flicking.css";
import VueScratchable from "vue-scratchable";
import VueDatePicker from '@mathieustan/vue-datepicker';
import '@mathieustan/vue-datepicker/dist/vue-datepicker.min.css';
import mitt from 'mitt'
const emitter = mitt();
window.toastr = require("toastr");


// const urlApi = 'http://192.168.200.226:8041/'; //URL TESTING BASE
const urlApi = 'https://casafortuna.com.ve/'; //URL PRODUCCION BASE
window.$ = window.jQuery = require('jquery');
Vue.prototype.$apiGeneral = urlApi + "triplegordoweb.Servicio.general/ServicioTGWebGeneral.svc/ServicioTGWebGeneral/";
Vue.prototype.$apiAdress = urlApi + "triplegordoweb.Servicio.juego/ServicioTGWebJuego.svc/ServicioTGWebJuego/";
Vue.prototype.$apiAdressUsuario = urlApi + "triplegordoweb.Servicio.Usuario/ServicioTGWebUsuario.svc/ServicioTGWebUsuario/";
Vue.prototype.$apiAdressCobranza = urlApi + "triplegordoweb.Servicio.cobranza/ServicioTGWebCobranza.svc/ServicioTGWebCobranza/";

Vue.config.performance = true;
Vue.use(CoreuiVue);
Vue.use(VueSweetalert2);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(Vuelidate);
Vue.use(VueToastr2);
Vue.use(VueFormWizard);
Vue.use(VueFlicking);
Vue.prototype.emitter = emitter;


Vue.use(VueDatePicker, {
  lang: 'es'
});
Vue.component("vue-scratchable", VueScratchable);
Vue.component("loading-overlay", Loading);
Vue.prototype.$bus = new Vue();
new Vue({
  el: "#app",
  router,
  store,
  icons,
  template: "<App/>",
  components: {
    App,
  },
});
