import Vue from "vue";
import Router from "vue-router";

// Containers
const TheContainer = () => import("@/containers/TheContainer");

// Views
const Dashboard = () => import("@/views/Dashboard");

// Views - Pages
const Page404 = () => import("@/views/pages/Page404");
const Page500 = () => import("@/views/pages/Page500");
const Login = () => import("@/views/pages/Login");
const Register = () => import("@/views/pages/Register");
const Index = () => import("@/views/pages/Index");

// Users
const UserAjustes = () => import("@/views/users/UserAjustes");
const UserPerfil = () => import("@/views/users/UserPerfil");


//CONSTANTE PARA REGISTRO DE PAGO SELECCION DE CARTON
const compra = () => import("@/views/compra/index");

const paypal = () => import("@/views/ticket/paypal");

//CONSTANTE PARA LOS TICKET
const ticket = () => import("@/views/ticket/index");

const play = () => import("@/views/play/index");

const validar = () => import("@/views/validar/index");

const pagar = () => import("@/views/payTicket/index");

const comprobante = () => import("@/views/comprobante/index");

const RetirarSaldo = () => import("@/views/RetirarSaldo/index");

const reporte = () => import("@/views/ReporteMovimiento/index");

const abonar = () => import("@/views/AbonarMonedero/index");

const movimiento = () => import("@/views/ProcesarMovimiento/index");


Vue.use(Router);

let router = new Router({
  mode: "hash", // https://router.vuejs.org/api/#mode
  linkActiveClass: "active",
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes(),
});

//validacion para val Auth
router.beforeEach((to, from, next) => {
  const usuarioValidado = sessionStorage.getItem("usuarioValidado");
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (usuarioValidado && usuarioValidado === "1") {
      next();
    } else {
      next({
        path: "/",
        params: { nextUrl: to.fullPath },
      });
    }
  } else {
    next();
  }
});

export default router;

function configRoutes() {
  return [
    {
      path: "/index",
      component: Index,
      alias: "/",
    },
    {
      path: "/",
      redirect: "/dashboard",
      name: "Home",
      component: TheContainer,
      children: [
        {
          path: "dashboard",
          name: "Dashboard",
          component: Dashboard,
          meta: {
            requiresAuth: true,
          },
        },

        {
          path: "users",
          meta: { label: "Users" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "/settings",
              name: "settings",
              component: UserAjustes,
              meta: {
                requiresAuth: true,
              },
            },
            {
              path: "/profile",
              name: "profile",
              component: UserPerfil,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        //RUTA PARA COMPRA CARTON
        {
          path: "jugar",
          meta: { label: "jugar" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: compra,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
         //RUTA TEST DE PAYPAL
         {
          path: "paypal",
          meta: { label: "paypal" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: paypal,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        //RUTA ABONAR MONEDERO
        {
          path: "abonar",
          meta: { label: "abonar" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: abonar,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        //RUTA Procesar Movimiento
        {
          path: "movimiento",
          meta: { label: "movimiento" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: movimiento,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        {
          path: "reporte",
          meta: { label: "reporte" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: reporte,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        //RUTA PARA TICKET
        {
          path: "ticket",
          meta: { label: "ticket" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: ticket,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        //Ruta para Retirar saldo
        {
          path: "RetirarSaldo",
          meta: { label: "RetirarSaldo" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: RetirarSaldo,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        {
          path: "play",
          meta: { label: "play" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: play,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        {
          path: "validar",
          meta: { label: "validar" },
          component: {
            render(c) {
              return c("router-view");
            },
          },
          children: [
            {
              path: "",
              component: validar,
              meta: {
                requiresAuth: true,
              },
            },
          ],
        },
        {
          path: "/pagar:obj",
          name: "pagar",
          component: pagar,
        },
        {
          path: "/comprobante:obj",
          name: "comprobante",
          component: comprobante,
        },
      ],
    },
    {
      path: "/pages",
      redirect: "/pages/404",
      name: "Pages",
      component: {
        render(c) {
          return c("router-view");
        },
      },
      children: [
        {
          path: "404",
          name: "Page404",
          component: Page404,
        },
        {
          path: "500",
          name: "Page500",
          component: Page500,
        },
      ],
    },
    {
      path: "/login",
      name: "Auth",
      component: Login,
    },
    {
      path: "/register",
      name: "Register",
      component: Register,
    },
    {
      path: "*",
      name: "404",
      component: Page404,
    },
  ];
}
