import { required } from "vuelidate/lib/validators"; 
import { SoloNumeros } from "@/_validations/ValidacionEspeciales"; 
export default () => { 
 return { 
    montoRetiroInput: { required,SoloNumeros } 
  }; 
}; 