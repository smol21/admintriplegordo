import { required, maxLength, minLength, sameAs } from "vuelidate/lib/validators";
import { passwordRgx } from '@/_validations/ValidacionEspeciales';

export default () => {
    return {
        old_password: {required, minLength: minLength(6), maxLength: maxLength(12), passwordRgx },
        password: {required, minLength: minLength(6), maxLength: maxLength(12), passwordRgx },
        password_confirmation: {required, minLength: minLength(6), maxLength: maxLength(12), sameAsPassword: sameAs('password')},
        select :{required} 
    }
}