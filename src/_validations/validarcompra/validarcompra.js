import { required, maxLength, minLength } from "vuelidate/lib/validators";
import { onlyNumber } from "@/_validations/ValidacionEspeciales";

export default () => {
  return {
    Operator_code: { required, onlyNumber, minLength: minLength(8), maxLength: maxLength(8) },
    serial: { required, onlyNumber, minLength: minLength(12), maxLength: maxLength(12) },
  };
};
