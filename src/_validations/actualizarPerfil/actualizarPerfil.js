import { required, maxLength, /* email,  */minLength, numeric } from "vuelidate/lib/validators";
import { notSeleccione, onlyLettersSpecial, emailSpecial as email } from '@/_validations/ValidacionEspeciales';

export default () => {
    return {
        // idUser: {},
        nameUser: { required, maxLength: maxLength(100), onlyLettersSpecial },
        lastNameUser: { required, maxLength: maxLength(100), onlyLettersSpecial },
        addressUser: { required, maxLength: maxLength(150) },
        stateUser : { required, notSeleccione }, 
        cityUser : { required, notSeleccione },
        emailUser : { required, maxLength: maxLength(75), email },
        phoneUser : { required, minLength: minLength(7), maxLength: maxLength(7), numeric },
    }
}