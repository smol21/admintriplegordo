import { required, /* email,  */minLength, maxLength, sameAs, numeric } from "vuelidate/lib/validators";
import { notSeleccione, passwordRgx, emailSpecial as email } from '@/_validations/ValidacionEspeciales';

export default {
    login: { required, numeric, minLength: minLength(7), maxLength: maxLength(8) },
    email : { required, maxLength: maxLength(75), email },
    password_new: { required, minLength: minLength(6), maxLength: maxLength(12), passwordRgx },
    password_confirmation: { required, minLength: minLength(6), maxLength: maxLength(12), sameAsPassword: sameAs('password_new') },
    caducidad: { required, notSeleccione },
}