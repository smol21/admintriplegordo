import { required, maxLength, minLength, numeric } from "vuelidate/lib/validators";
import { passwordRgx } from '@/_validations/ValidacionEspeciales';

export default {
    login: { required, numeric, minLength: minLength(7), maxLength: maxLength(8) },
    password: { required, minLength: minLength(6), maxLength: maxLength(12), passwordRgx },  
}