import { required, maxLength, minLength, numeric } from "vuelidate/lib/validators";

export default {
    codigo: { required, numeric, minLength: minLength(6), maxLength: maxLength(6) },
}