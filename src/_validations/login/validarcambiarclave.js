import { required, minLength, maxLength, sameAs, numeric } from "vuelidate/lib/validators";
import { notSeleccione, passwordRgx } from '@/_validations/ValidacionEspeciales';

export default {
    login: { required, numeric, minLength: minLength(7), maxLength: maxLength(8) },
    password: { required, minLength: minLength(6), maxLength: maxLength(12), passwordRgx },  
    password_new: { required, minLength: minLength(6), maxLength: maxLength(12), passwordRgx },
    password_confirmation: { required, minLength: minLength(6), maxLength: maxLength(12), sameAsPassword: sameAs('password_new') },
    caducidad: { required, notSeleccione },
}