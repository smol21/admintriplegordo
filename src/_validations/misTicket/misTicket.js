import { CompareNumber } from "@/_validations/ValidacionEspeciales";

export default () => {
    return {
        start_date : { CompareNumber }
    };
  };