import axios from "axios";

  //Metodos:
  function getBadge(status) {
    switch (status) {
      case 'ACTIVO':
        return 'success'
      case 'INACTIVO':
        return 'danger'
    }
  }

  function hasError(field){
    return field.$dirty ? !field.$error : null;
  }

  //Computeds
  function desactivado() {
    return this.$v.$invalid
  }

  function statusSelectColor(){
    return this.Status === 1;
  }

  //Esta funcion se debe colocar en todos los catch
  function globalError (isToast = false){
    const msg = ["Solicitud no procesada. Comuníquese con el administrador", "Solicitud no procesada"];
    if (isToast) this.$swal.fire(
      {
        position: "top-end",
        toast: true,
        icon: "error",
        title: msg[0],
        showConfirmButton: false,
        timer: self.getMsgSeconds(msg[0]),
        timerProgressBar: true,
      });
    else this.$swal.fire(
    {
      position: "top-end",
      toast: true,
      icon: "error",
      title: msg[1],
      showConfirmButton: false,
      timer: self.getMsgSeconds(msg[1]),
      timerProgressBar: true,
    });
  }

  // Characters min: 14, max: 160. Seconds min: 2.5, max: 8
  function getMsgSeconds (str = '') {
    const len = str.length < 14 ? 14 : str.length > 160 ? 160 : str.length;
    const seconds = (( ( len * 55000 ) + 2880000 ) / 1460);
    return Math.round(seconds)
  }

  function getTicketData (data = null){
    const sectionObj = (case_, keys, i, key) => {
      const decodedDataKey = window.atob(data[key]);
      let obj = {};
      switch (case_) {
        case 1: 
          obj = { 
            [`pm${i + 1}1`]: decodedDataKey.slice(0,3), 
            [`pm${i + 1}2`]: decodedDataKey.slice(3) 
          }; 
          break;
        case 2: 
          obj = { 
            [`tt${i + 1}`]: decodedDataKey 
          }; 
          break;
        case 3: 
          obj = { 
            [`sd${i + 1}1`]: decodedDataKey.slice(0,3), 
            [`sd${i + 1}2`]: decodedDataKey.slice(3) 
          }; 
          break;
        case 4: 
          const ind = !i ? [0, 3] : i < 2 ? [3, -3] : [-3]; 
          obj = { 
            [`i${i + 1}1`]: decodedDataKey.slice(...ind).slice(0,1), 
            [`i${i + 1}2`]: decodedDataKey.slice(...ind).slice(1,-1), 
            [`i${i + 1}3`]: decodedDataKey.slice(...ind).slice(-1) 
          }; 
          break;
      }
      return { ...keys, ...obj };
    };

    return {
      image: require('@/assets/carton2.png'),
      images: [
        !(+data['estatusParMil']) ? require("@/assets/cartonImg1.png") : null,
        !(+data['estatusTripleTer']) ? require("@/assets/cartonImg2.png") : null,
        !(+data['estatusSuperDup']) ? require("@/assets/cartonImg3.png") : null,
        !(+data['estatusInstantaneo']) ? require("@/assets/cartonImg4.png") : null,
      ],
      ticket: {
        borderColor: data['colorBorde'] || '#041a72',
        code: window.atob(data['tripleAbierto']),
        numeroFactura: data['numeroFactura'],
        numeroSorteo: data['codigoSorteo'],
        scratches: [
          // PAR MILLONARIO 
          ['parMillonarioA', 'parMillonarioB'].reduce( (keys, key, i) => sectionObj(1, keys, i, key), {} ),
          // TRIPLE TERMINAL
          ['tripleTerminalA', 'tripleTerminalB', 'tripleTerminalC'].reduce( (keys, key, i) => sectionObj(2, keys, i, key), {} ),
          // SUPER DUPLETA
          ['superDupleta1', 'superDupleta2', 'superDupleta3'].reduce( (keys, key, i) => sectionObj(3, keys, i, key), {} ),
          // INSTANTANEO
          Array(3).fill('instantaneo').reduce( (keys, key, i) => sectionObj(4, keys, i, key), {} ),
        ]
      }
    };
  }

  async function globalConsultarSaldo(show = true, emit = false) {
    const url_saldo = this.$apiAdressUsuario;
    const tokUsuario = sessionStorage.getItem("tokenUsuario");
    const tokenSesion = localStorage.getItem("tokenSesion");
    const tipoUsuario = sessionStorage.getItem("tipoUsuario") || '';
    const codigoAplicacion = "MDE=";
    const self = this;

    const result = async () => {
      try {
        if (tipoUsuario !== '02') return { doNothing: true };
        const resp = await axios
          .get(
            url_saldo + "ConsultarSaldo" + "/" + codigoAplicacion + "/" + tokUsuario,
            {
              headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + tokenSesion,
              },
            }
          );
        return {
          resp
        };  
      } catch (err) {
        const msg = err?.response?.data?.message?.description ||
            err?.responseJSON?.message?.description ||
            err?.responseJSON?.mensaje?.description ||
            err?.response?.statusText || '';
        if ( show && tipoUsuario === '02' && msg)
          self.$swal.fire({
            position: "top-end",
            toast: true,
            icon: "warning",
            title: msg,
            showConfirmButton: false,
            timer: self.getMsgSeconds(msg),
            timerProgressBar: true,
          });
        else if (show && tipoUsuario === '02') self.globalError(true);
        if (show && tipoUsuario === '02' && err.response?.data?.message?.code === "008") self.clearSession();
        return {
          err
        };
      }
    };
    
    const result_ = await result();

    if (result_.resp) {
      console.log("response",result_ );
      if (result_.resp.data.mensaje.code === "000") {
        const saldo_ = result_.resp.data.datos.montoDisponible;
        const montoRetiro_ = result_.resp.data.datos.montoRetiro;
        sessionStorage.setItem("saldoDisponible", saldo_);
        sessionStorage.setItem("montoRetiro",montoRetiro_);
        if (emit) self.emitter.emit('Refrescar', saldo_);
        return {
          saldo: saldo_,
          montoRetiro: montoRetiro_,
          isSuccess: true,
        }
      }
    } else return result_.doNothing ? 'doNothing' : result_.err;
  }

  function clearSession() {
    const self = this;
    sessionStorage.clear(); 
    setTimeout(() => {
      self.$router.push({ path: "/" });
    }, 3000);
  };

  export default {
    methods: {
      getBadge,
      hasError,
      statusSelectColor,
      globalError,
      getMsgSeconds,
      getTicketData,
      globalConsultarSaldo,
      clearSession,
    },
    computed: {
      desactivado
    }
  }