import axios from "axios";
  //Data
  function data() {
    return {
      sessionTimeout: [],
      initialized: true,
      alertShowed: false,
    }
  }

  //Metodos
  async function extendSessionTimeout() {
    const self = this;
    let url = this.$apiAdressUsuario + "MantenerSesion";
    const tokenUser = sessionStorage.getItem('tokenUsuario');

    const awaitSession = async (tokenUsuario) => {
      try {
        const resp = await axios.put(url, { tokenUsuario });
        return resp;
      } catch (err) {
        console.log(err);
        const msg = err?.response?.data?.message?.description ||
          err?.responseJSON?.message?.description ||
          err?.responseJSON?.mensaje?.description ||
          err?.response?.statusText || '';
        if (msg)
          self.$swal.fire({
            position: "top-end",
            toast: true,
            icon: "warning",
            title: msg,
            showConfirmButton: false,
            timer: self.getMsgSeconds(msg),
            timerProgressBar: true,
          });
        else self.globalError();
        if (err.response?.data?.message?.code === "008") {
          setTimeout(() => {
            self.closeSession();
          }, self.getMsgSeconds(msg));
        }
        return null;
      }
    };

    const response = await awaitSession(tokenUser);

    if (response) {
      if (self.sessionTimeout) {
        const theTO = setTimeout(() => {
          if (!self.alertShowed) self.sessionTimeoutAlert();
        }, (+sessionStorage.getItem("tiempoSesion") - +sessionStorage.getItem("tiempoAlerta")) * 1000);
        self.sessionTimeout.push(theTO);
      }
      console.log("Responses extender sesion", response);
      const now_ = new Date();
      sessionStorage.setItem("comienzoSesionDate", now_.toISOString());
    }
  }

  async function sessionTimeoutAlert(showAlert = true) {
    this.alertShowed = true;
    console.log("entro en el metodo sesión");
    const self = this;
    const time_out = setTimeout(() => {
      self.alertShowed = false;
      this.$swal.close();
      this.sessionLogout();
    }, +sessionStorage.getItem("tiempoAlerta") * 1000);
    const theSwal = !showAlert 
      ? {
        isConfirmed: true,
      } 
      : await this.$swal.fire({
        title: '¡Su sesión está por vencer!',
        text: '¿Desea extender la sesión?',
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "CONFIRMAR",
        cancelButtonText: "CANCELAR",
      });

    this.alertShowed = false;
    if (theSwal.isConfirmed) {
      clearTimeout(time_out);
      await this.extendSessionTimeout();
    }
  }

  function callSessionTimeout() {
    const self = this;
    if (
      sessionStorage.getItem("usuarioValidado") && sessionStorage.getItem("usuarioValidado") === '1' &&
      sessionStorage.getItem("tiempoAlerta") &&
      sessionStorage.getItem("tiempoSesion")
    ) {
      if (this.sessionTimeout) {
        const thaTimeOut = setTimeout(() => {
          console.log("entro en el mounted");
          if (!self.alertShowed) self.sessionTimeoutAlert();
        }, (+sessionStorage.getItem("tiempoSesion") - +sessionStorage.getItem("tiempoAlerta")) * 1000);
        this.sessionTimeout.push(thaTimeOut);
      }
    }
  }

  function closeSession(notNav = false) {
    this.clearSessionTimeout();
    sessionStorage.clear(); 
    if (!notNav) this.$router.push({ path: "/" });
  };

  function sessionLogout(notNavigate = false) {
    let self = this;
    const tokenSesion = localStorage.getItem("tokenSistema");
    const tokenUsuario = sessionStorage.getItem("tokenUsuario");

    const hasCombinations = sessionStorage.getItem('hasCombinations');
    if (hasCombinations && +hasCombinations === 1) this.emitter.emit('setTicketsFree', {});

    console.log("logout");
    if (tokenUsuario) axios
      .delete(
        this.$apiAdressUsuario +
        "CerrarSesion/" +
        tokenUsuario, 
        {
            headers: {
              "Content-Type": "multipart/form-data",
              Authorization: "Bearer " + tokenSesion,
            },
        }
      )
      .then(function (response) {
        if (response && response.data.message.code === "000") {
          console.log("cerrar", response);
          self.closeSession(notNavigate);
        }
      })
      .catch(function (err) {
        console.log(err);
        const isSessionNotActive = err?.response?.data?.message?.code === "008";
        if (isSessionNotActive) self.closeSession(notNavigate);  
        else {          
          self.emitter.emit('problemClosingSession', false);
          if (hasCombinations && +hasCombinations === 1) self.$router.push({ path: "dashboard" });
          const msg = err?.response?.data?.message?.description ||
            err?.responseJSON?.message?.description ||
            err?.responseJSON?.mensaje?.description ||
            err?.response?.statusText || '';
          if (msg)
            self.$swal.fire({
              position: "top-end",
              toast: true,
              icon: "warning",
              title: msg,
              showConfirmButton: false,
              timer: self.getMsgSeconds(msg),
              timerProgressBar: true,
            });
          else self.globalError();
        }
      });
  }

  function clearSessionTimeout() {
    if (this.sessionTimeout) this.sessionTimeout.forEach(ssTO => clearTimeout(ssTO));
    delete this.sessionTimeout;
    this.sessionTimeout = [];
  }

  function clearAndCallSessionTimeout(makeCall = true) {
    this.clearSessionTimeout();
    if (makeCall) this.callSessionTimeout();
  }

  export default {
    data,
    methods: {
      extendSessionTimeout,
      sessionTimeoutAlert,
      callSessionTimeout,
      sessionLogout,
      clearSessionTimeout,
      clearAndCallSessionTimeout,
      closeSession,
    },
    destroyed(){
        this.clearSessionTimeout();
    },
  }