import { logo } from './logo';
import { 
  linearSet as icons, 
  solidSet as iconsSolid,
  flagSet as flags 
} from '@coreui/icons-pro';

export const iconsSet = Object.assign(
  {},
  { logo },
  icons,
  flags,
  iconsSolid
)